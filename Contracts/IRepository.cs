using DomainApplication.Entities;
using Microsoft.AspNetCore.Mvc;

namespace DDDInfrastructure.Contracts
{
    public interface IRepository
    {
        ICollection<Post> GetPosts();
        Post GetPostById(int id);
        Post CreatePost(Post post);
        ActionResult<Post> UpdatePost(int id, PostDTO postDTO);
        ActionResult<int> DeletePost(int id);
    }
}