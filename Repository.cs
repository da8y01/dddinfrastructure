﻿using Microsoft.AspNetCore.Mvc;
using DomainApplication.Entities;
using Microsoft.EntityFrameworkCore;
using DDDInfrastructure.Contracts;

namespace DDDInfrastructure
{
    public class Repository : ControllerBase, IRepository, IDisposable
    {

            //var post = await _context.Posts.FindAsync(id);
        public ICollection<Post> GetPosts()
        {
            ICollection<Post> lista=new List<Post>();
            try
            {
                using (var context = new PostContext())
                {
                    lista = context.Posts.ToList();
                }

                return lista;
            }
            catch (Exception e)
            {
                throw new Exception(e.ToString());
            }
        }

        public Post GetPostById(int id)
        {
            Post post;
            try
            {
                using (var context = new PostContext())
                {
                    post = context.Posts.Find(id);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.ToString());
            }
            return post;
        }

        public Post CreatePost(Post post)
        {
            try
            {
                int saveTask;
                using (var context = new PostContext())
                {
                    context.Posts.Add(post);
                    saveTask = context.SaveChanges();
                }
                return post;
                //return saveTask;
            }
            catch (Exception e)
            {
                throw new Exception(e.ToString());
            }
        }

        public ActionResult<Post> UpdatePost(int id, PostDTO postDTO)
        {
            Post postResult;

            if (id != postDTO.Id)
            {
                return BadRequest();
            }

            try
            {
                using (var context = new PostContext())
                {
                    postResult = context.Posts.Find(id);
                    if (postResult == null)
                    {
                        return NotFound();
                    }
                    postResult.Content = postDTO.Content;
                    context.SaveChanges();
                    return postResult;
                }
            }
            catch (DbUpdateConcurrencyException) when (!PostExists(id))
            {
                return NotFound();
            }
            catch (Exception e)
            {
                throw new Exception(e.ToString());
            }
        }

        public ActionResult<int> DeletePost(int id)
        {
            //ActionResult<int> result;
            using (var context = new PostContext())
            {
                var post = context.Posts.Find(id);
                if (post == null)
                {
                    return NotFound();
                }

                context.Posts.Remove(post);
                context.SaveChanges();
                return Ok();
            }
        }

        private bool PostExists(int Id)
        {
            using (var context = new PostContext())
            {
                return context.Posts.Any(e => e.Id == Id);
            }
        }

        // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        ~Repository()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(false);
        }

        private void Dispose(bool disposing)
        {
            GC.SuppressFinalize(this);
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            
            GC.SuppressFinalize(this);
        }
    }
}