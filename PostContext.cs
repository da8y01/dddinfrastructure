﻿/*
https://www.learnentityframeworkcore.com/walkthroughs/existing-database

dotnet ef dbcontext scaffold "Data Source=DESKTOP-2AS75HO;Initial Catalog=DecoWraps1;Persist Security Info=True;User ID=sa;Password=SQLServer2022." Microsoft.EntityFrameworkCore.SqlServer -o Models

dotnet ef dbcontext scaffold "Datasource=C:\ASPNET\APIApp\webbookmarks.db3" -o Models Microsoft.EntityFrameworkCore.Sqlite -c "APIAppDbContext" -f -a
The “-o” command line argument is for specifing the output directory, “-c” is for specifing the DbContext class name, “-f” is for forcing the class generation, even if the classes exists. And the last “-a” is for using DataAnnotation attributes to configure the model, instead of fluent API.

The -o option (or --output-dir) specifies the directory where the class files will be generated. If it is omitted, the class files will be generated in the project directory (where the .csproj file is located).

The DbContext class will take the name of the database plus "Context", You can override this using the -c or --context option e.g.

> dotnet ef dbcontext scaffold "Server=.\;Database=AdventureWorksLT2012;Trusted_Connection=True;" Microsoft.EntityFrameworkCore.SqlServer -o Model -c "AdventureContext"

The resulting model is configured using the Fluent API by default, which is the recommended approach. The configurations are placed in the generated context's OnModelCreating method. However, if you prefer to use Data Annotations for configuration, you can use the -d or --data-annotations switch:

dotnet ef dbcontext scaffold "Server=.\;Database=AdventureWorksLT2012;Trusted_Connection=True;" Microsoft.EntityFrameworkCore.SqlServer -d 
Since Data Annotations only cover a subset of configuration options, you are likely to find that the Fluent API has also been used to configure the model.

If you need to re-scaffold the model after database schema changes have been made, you can do so by specifying the -f or --force option e.g.:

dotnet ef dbcontext scaffold "Server=.\;Database=AdventureWorksLT2012;Trusted_Connection=True;" Microsoft.EntityFrameworkCore.SqlServer --force
All of the class files will be overwritten, which means that any amendments that you might have made to them e.g. adding attributes or additional members, will be lost. You can mitigate this by opting to use the Fluent API for configuration and using separate configuration classes. In addition, you can use partial classes to declare additional properties that don't map to columns in the database tables.
*/

using DomainApplication.Entities;
using Microsoft.EntityFrameworkCore;

namespace DDDInfrastructure
{
    public class PostContext: DbContext
    {
        public PostContext() { }

        public PostContext(DbContextOptions<PostContext> options)
        : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=DESKTOP-2AS75HO;Initial Catalog=DecoWraps1;Persist Security Info=True;MultipleActiveResultSets=True;User ID=sa;Password=SQLServer2022.");
        }

        public DbSet<Post> Posts { get; set; } = null!;
    }
}
